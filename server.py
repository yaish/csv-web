from flask import Flask, request, Response, render_template
import smtplib

app = Flask(__name__, static_folder='assets')


@app.route(r'/my-csv', methods=['GET'])
def view():
    return render_template('index.html')


@app.route(r'/contact', methods=['POST'])
def contact_me():
    email, name, message = get_data()
    smtp_sender = connect_to_smtp()
    send_email(smtp_sender=smtp_sender, message=message, email_to_send=email)
    return Response({'status': 'ok'}, status=200)


def get_data():
    email = request.form['email']
    message = request.form['message']
    name = request.form['name']
    final_message = 'Subject: {}\n\n{}'.format('message from {}'.format(name), message)
    return email, name, final_message


def connect_to_smtp():
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login('Your gmail username Here', 'your gmail password here')
    return server


def send_email(smtp_sender, message, email_to_send):
    smtp_sender.sendmail('yaish2k@gmail.com', email_to_send, message)
    smtp_sender.quit()


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=9000)
